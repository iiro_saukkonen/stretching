var elixir = require('laravel-elixir');

require('laravel-elixir-vueify');

elixir(function(mix) {
    mix.browserify('main.js');
    mix.copy('node_modules/material-design-lite/dist/material.min.css', 'public/css/material.min.css');
    mix.copy('node_modules/material-design-lite/dist/material.min.js', 'public/js/material.min.js');
});